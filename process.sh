#!/bin/bash

for d in sg1 sg2
do

   dir=/host/cam/${d}/${1}/${2}/${3}

   if [ -d ${dir} ]
   then
      cd ${dir}

      rm -f *.gif
      rm -f *.mp4
      find ./ -name *.jpg -size -6000c -delete -mtime +1
      cat *.jpg | ffmpeg -framerate 10 -f image2pipe -i - temp.mp4
      ffmpeg -i temp.mp4 -i /host/cam/lala.mp3 -shortest -c copy -map 0:v:0 -map 1:a:0 timelapse.mp4
      ffmpeg -i timelapse.mp4 timelapse.gif
      rm -f temp.mp4
   fi

done
