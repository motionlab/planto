FROM ubuntu:21.10

ENV DEBIAN_FRONTEND="noninteractive" 

RUN  useradd -r -u 1001 appuser

# Basic stuff
RUN apt update     && \
    apt upgrade -y && \
    apt install -y curl mc vim python pip ffmpeg

COPY requirements.txt requirements.txt
RUN  pip3 install -r  requirements.txt

RUN mkdir /app
RUN mkdir /home/appuser
RUN chown appuser /app
RUN chown appuser /home/appuser

WORKDIR /app
USER    appuser

ADD process.sh  /app/process.sh
ADD generate.py /app/generate.py
ADD templates   /app/templates
