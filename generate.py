import sys
import jinja2
import datetime
from   os                     import walk
from   os                     import path
from   calendar               import monthrange
from   dateutil.relativedelta import relativedelta

iconpath = "https://icons.23-5.eu"

year  = datetime.date.today().year
month = datetime.date.today().month

if len(sys.argv) < 2:
    print("Missing parameter must be x or year month")
    sys.exit()

if sys.argv[1] == "x":
    y     = datetime.date.today() - relativedelta(days = 1)
    year  = y.year
    month = y.month
else:
    year  = int(sys.argv[2])
    month = int(sys.argv[3])

month_name = {}
month_name[ 1] = "Ja"
month_name[ 2] = "Feb"
month_name[ 3] = "Mar"
month_name[ 4] = "Apr"
month_name[ 5] = "May"
month_name[ 6] = "Jun"
month_name[ 7] = "Jul"
month_name[ 8] = "Aug"
month_name[ 9] = "Sep"
month_name[10] = "Oct"
month_name[11] = "Nov"
month_name[12] = "Dec"

vars = {}
vars['title_date'] = "%s - %s" % (year, month)
vars['year']       = year
vars['month']      = month
vars['month_name'] = month_name[month]
vars['iconpath']   = iconpath

today_dt = datetime.datetime(year, month, 1, 12, 12, 12)

links = {}
links['month_to_last_year']   = (today_dt - relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_next_year']   = (today_dt + relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_last_month']  = (today_dt - relativedelta(months = 1)).strftime("%Y%m.html")
links['month_to_next_month']  = (today_dt + relativedelta(months = 1)).strftime("%Y%m.html")




for (dirpath, dirnames, filenames) in walk("/host/cam/sg1/{}/{:02d}".format(year, month)):
    break

monthinfo_sg1 = []
dirnames.sort()

for d in dirnames:
    data = {}

    for (_, _, filenames) in walk("/host/cam/sg1/{}/{:02d}/{}".format(year, month, d)):
        break

    if len(filenames) == 0:
        continue

    displayfilename = None
    for i in filenames:
        if i.endswith(".jpg"):
            displayfilename = i

    if displayfilename == None:
        continue

    data['day']   = d
    data['image'] = "cam/sg1/{}/{:02d}/{}/{}".format(year, month, d, displayfilename)
    data['video'] = "cam/sg1/{}/{:02d}/{}/timelapse.mp4".format(year, month, d)
    data['gif']   = "cam/sg1/{}/{:02d}/{}/timelapse.gif".format(year, month, d)

    monthinfo_sg1.append(data)      




for (dirpath, dirnames, filenames) in walk("/host/cam/sg2/{}/{:02d}".format(year, month)):
    break

monthinfo_sg2 = []
dirnames.sort()

for d in dirnames:
    data = {}

    for (_, _, filenames) in walk("/host/cam/sg2/{}/{:02d}/{}".format(year, month, d)):
        break

    if len(filenames) == 0:
        continue

    displayfilename = None
    for i in filenames:
        if i.endswith(".jpg"):
            displayfilename = i

    if displayfilename == None:
        continue

    data['day']   = d
    data['image'] = "cam/sg2/{}/{:02d}/{}/{}".format(year, month, d, displayfilename)
    data['video'] = "cam/sg2/{}/{:02d}/{}/timelapse.mp4".format(year, month, d)
    data['gif']   = "cam/sg2/{}/{:02d}/{}/timelapse.gif".format(year, month, d)

    monthinfo_sg2.append(data)      






templateLoader = jinja2.FileSystemLoader(searchpath="templates")
templateEnv    = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE  = "month.html"
template       = templateEnv.get_template(TEMPLATE_FILE)
outputText     = template.render(vars=vars, links=links, monthinfo_sg1=monthinfo_sg1, monthinfo_sg2=monthinfo_sg2)

with open(today_dt.strftime("/host/%Y%m.html"), 'w') as the_file:
    the_file.write(outputText)
